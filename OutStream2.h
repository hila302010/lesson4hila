#pragma once
#include <stdio.h>
#include <stdlib.h>
namespace msl
{
	class OutStream
	{
	public:
		// constactor:
		OutStream();
		// destractor:
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());

		FILE * file;
	};

	void endline();
	void endline(FILE* _newFile);
}