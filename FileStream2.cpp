#include "FileStream.h"
using namespace msl;
FileStream & FileStream::operator<<(const char * str)
{
	fprintf(this->file, str);
	return *this;
}

FileStream & FileStream::operator<<(int num)
{
	char str[BUFSIZ];
	sprintf_s(str, "%d", num);
	*this << (str);
	return *this;
}

FileStream & FileStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}

void msl::endline(FILE * _newFile)
{
	fprintf(_newFile, "\n");
}