#pragma once
#include "outStream.h"
namespace msl
{
	class FileStream : public OutStream {
	public:
		FileStream & operator<<(const char *str);
		FileStream& operator<<(int num);
		FileStream& operator<<(void(*pf)());
	};
}